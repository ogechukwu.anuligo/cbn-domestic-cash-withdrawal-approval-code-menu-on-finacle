var funcCode =  ""
function printBlock()
{
	writeCustomHeader("apcode_det");
	with (document){
	write('<table border="0" cellspacing="0" cellpadding="0" class="ctable">');
	write('<tr>');
	write('<td>');
	write('<table border="0" cellspacing="0" cellpadding="0">');
	write('<tr>');
	write('<td class="page-heading">' + jspResArr.get("FLT090337") + '</td>');
	write('</tr>');
	write('</table>');
	write('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
	write('<tr>');
	write('<td class="textlabel"> </td>');
	write('<td class="textfielddisplaylabel"> </td>');
	write('<td class="columnwidth">&nbsp; </td>');
	write('<td class="textlabel"> </td>');
	write('<td class="textfielddisplaylabel"> </td>');
	write('</tr>');
	write('</table>');
	write('<br />');
	write('<!-- DETAILSBLOCK-BEGIN -->');
	write('<table border="0" cellpadding="0" cellspacing="0" width="100%">');
	write('<tr>');
	write('<td valign="top">');
	write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">');
	write('<tr>');
	write('<td>');
	write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="innertable">');
	write('<tr>');
	write('<td>');
	write('<table width="100%" border="0" cellpadding="0" cellspacing="0" class="innertabletop1">');
	write('<tr>');
	write('<td height="25" colspan="5" align="right">');
	write('<table border="0" cellspacing="0" cellpadding="0">');
	write('<tr>');
	write('<td align="right">');
	write('<a href="javascript:showHelpFile(\'det_help.htm\');" id="sLnk1">');
	write('<img  hotKeyId="finHelp" src="../Renderer/images/'+applangcode+'/help.gif" width="17" height="17" vspace="1" border="0" />');
	write('</a>');
	write('</td>');
	write('</tr>');
	write('</table>');
	write('</td>');
	write('</tr>');

	write('<tr>');
	write('<td class="textlabel" style="height: 15px">' + jspResArr.get("FLT090008") + '</td>');
	write('<td class="textfield">');
	write('<input hotKeyId="search1" type="text" name="' + subGroupName + '.acctNum" id="acctNum" ' + apcodeProps.get("acctNum_ENABLED") + '  onChange="javascript:return apcode_det_ONCHANGE1(this,this,\'acctName\',\'sol\',\'crncyCode\',true,\'VALACCTID\');"">');
	write('<a href="javascript:fnshowAccountIdList(document.forms[0].acctNum)" id="sLnk4">');
    write('<img border="0" height="17" hotKeyId="search1" src="../Renderer/images/'+applangcode+'/search_icon.gif" width="16">');
	write('<input class="label" name="sol" size="3" maxlength="5" fds= "Y" disabled>');
	write('<input class="label" name="crncyCode" size="2" maxlength="3" fds= "Y" disabled>');
	write('<input class="label" name="acctName" size="20" maxlength="25" fds= "Y" disabled>');
	write('<td width="20%" class="textlabel">' + jspResArr.get("FLT090338") + '</td>');
	write('<td width="10%" class="textfield"><input type="text" class="textfieldfont" id="Tranamt" '+apcodeProps.get("Tranamt_ENABLED")+' name="' + subGroupName + '.Tranamt" style="width: 170px;"/></td>');
	write('<td width="10%">&nbsp;</td>')
	write('</tr>');	
	write('</td>');
	write('</tr>');


	write('<tr>');
	write('<td width="20%" class="textlabel">' + jspResArr.get("FLT090340") + '</td>');
	write('<td width="10%" class="textfield"><input type="text" class="textfieldfont" id="solId" '+apcodeProps.get("solId_ENABLED")+' name="' + subGroupName + '.solId" style="width: 170px;"/></td>');
	write('<td width="10%">&nbsp;</td>')
	write('</td>');
	write('</tr>');
	
	write('<tr>');
	write('<td class="textlabel" style="height: 15px">' + jspResArr.get("FLT090344") + '</td>');
	write('<td class="textfield">');
	write('<input type="text" class="textfieldfont" id="approver" '+apcodeProps.get("approver_ENABLED")+' name="' + subGroupName + '.approver" style="width: 170px;"/>');
	write('<td width="20%" class="textlabel">' + jspResArr.get("FLT090350") + '</td>');
	write('<td width="10%" class="textfield"><input type="text" class="textfieldfont" id="ApprovalVal" '+apcodeProps.get("ApprovalVal_ENABLED")+' name="' + subGroupName + '.ApprovalVal" style="width: 170px;"/></td>');
	write('<td width="10%">&nbsp;</td>')
	write('</tr>');	
	write('</td>');
	write('</tr>');	
	write('<tr>');
	
	write('</tr>');	
	write('</td>');
	write('</tr>');

	
	write('</tr>');
	write('</table>');
	write('</td>');
	write('</tr>');
	write('</table>');
	write('</td>');
	write('</tr>');
	write('</table>');
	write('</td>');
	write('</tr>');
	write('</table>');
	write('<!-- DETAILSBLOCK-END -->');
	write('</td>');
	write('</tr>');
	write('</table>');
	} //End with()
} //End function

function printFooterBlock()
{
	with (document) {
	if ((sReferralMode == 'I')||(sReferralMode == 'S')){
	write('<div align="left" class="ctable">');
	if (sReferralMode == 'S'){
	write('<input type="button" class="Button" id="Submit" value="'+jspResArr.get("FLT000193")+ '" onClick="javascript:return doRefSubmit(this);" hotKeyId="Submit" >');
	}
	writeRefFooter();
	write('<input type="button" class="Button" id="_BackRef_" value="'+jspResArr.get("FLT001721")+ '" onClick="javascript:return doSubmit(this.id);" hotKeyId="Cancel" >');
	write('</div>');
	}else{
	if(funcCode !='I'){
	write('<div class="ctable">');
	write('<input id="Submit" name="Submit" type="button" class="button"	onClick="javascript:return apcode_det_ONCLICK1(this,this);"" value="' + jspResArr.get("FLT000193") + '" hotKeyId="Submit">');
	write('<input id="Cancel" name="Cancel" type="button" class="button" value="' + jspResArr.get("FLT001721") + '"	onClick="javascript:return apcode_det_ONCLICK3(this,this.id);"" hotKeyId="Cancel">');
	}else{
	write('<div class="ctable">');
	write('<input class="button" type="button" id="Back" value="'+jspResArr.get("FLT026526")+ '" onClick="javascript:return doSubmit(this.id)" hotKeyId="Ok">');
	}
	writeFooter();
	write('</div>');
	}
	} //End with()
}//End function


function fnOnLoad()
{
//alert("am here");
	var ObjForm = document.forms[0];

	pre_ONLOAD('apcode_det',this);

	var funcName = "this."+"locfnOnLoad";
	if(eval(funcName) != undefined){
		eval(funcName).call(this);
	}

	fnPopulateControlValues();

	//if(funcCode =='V' || funcCode =='I' || funcCode =='D' || funcCode =='U' ||  funcCode =='X' || sReferralMode =='I' || sReferralMode =='S'){
		//fnDisableFormDataControls('V',ObjForm,0);
	//}
	fnPopUpExceptionWindow(ObjForm.actionCode);
	if((typeof(WF_IN_PROGRESS) != "undefined") && (WF_IN_PROGRESS == "PEAS")){
		checkCustErrExecNextStep(Message);
	}

	post_ONLOAD('apcode_det',this);
}

function fnCheckMandatoryFields()
{
	var ObjForm = document.forms[0];

	return true;
}

function fnPopulateControlValues() 
{
	var ObjForm = document.forms[0];

	ObjForm.acctNum.value = acctNum;
	ObjForm.Tranamt.value = Tranamt;
	ObjForm.solId.value = solId;
	ObjForm.approver.value = approver;
	ObjForm.ApprovalVal.value = ApprovalVal;
	
}


function apcode_det_ONCLICK1(obj,p1)
{
	var retVal = "";
	if (preEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	if ((retVal =  fnValAndSubmit(p1)) == false) {
		return false;
	}
	if (postEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	return (retVal == undefined) ? true : retVal;
}

function apcode_det_ONCLICK2(obj,p1)
{
	var retVal = "";
	if (preEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	if ((retVal =  fnValAndSubmit(p1)) == false) {
		return false;
	}
	if (postEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	return (retVal == undefined) ? true : retVal;
}

function apcode_det_ONCLICK3(obj,p1)
{
	var retVal = "";
	if (preEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	if ((retVal =  doSubmit(p1)) == false) {
		return false;
	}
	if (postEventCall('apcode_det',obj,'ONCLICK') == false) { 
		return false;
	}
	return (retVal == undefined) ? true : retVal;
}

	
function apcode_det_ONCHANGE1(obj,p1,p2,p3,p4,p5,p6)
{
	var retVal = "";
	if (pre_ONCHANGE('apcode_det',obj) == false) {
			return false;
	}
	if ((retVal = fnCommonFetchAcctDtls(p1,p2,p3,p4,p5,p6)) == false) {
			return false;
	}
	if (post_ONCHANGE('apcode_det',obj) == false) {
			return false;
	}
	
	if(objForm.nochqlev.value==""){
		alert("Number of Cheque Leaves Must be entered");
      	 	objForm.nochqlev.focus();
			document.getElementById("nochqlev").focus();
			document.getElementById("acctNum").value = "";
			return false;
	}
   fnOnLoadControlNumber();
	return (retVal == undefined) ? true : retVal;
	

}